package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Panel;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.font.GlyphVector;
import java.awt.geom.Rectangle2D;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import generation.Wall;

/**
 * Add functionality for double buffering to an AWT Panel class.
 * Used for drawing a maze.
 * 
 * @author Peter Kemper
 *
 */
public class MazePanel extends Panel implements P5Panel  {
	private static final long serialVersionUID = 2787329533730973905L;
	/* Panel operates a double buffer see
	 * http://www.codeproject.com/Articles/2136/Double-buffer-in-standard-Java-AWT
	 * for details
	 */
	// bufferImage can only be initialized if the container is displayable,
	// uses a delayed initialization and relies on client class to call initBufferImage()
	// before first use
	private Image bufferImage;  
	private java.awt.Graphics2D graphics; // obtained from bufferImage, 
	// graphics is stored to allow clients to draw on the same graphics object repeatedly
	// has benefits if color settings should be remembered for subsequent drawing operations
	/**
	 * from FPV
	 */
	/**
	 * From Wall
	 */
	private Color col;
	private int int_col;
	
	static final Color greenWM = Color.decode("#115740");
	static final Color goldWM = Color.decode("#916f41");
	static final Color yellowWM = Color.decode("#FFFF99");
	/**
	 * Constructor. Object is not focusable.
	 */
	public MazePanel() {
		setFocusable(false);
		bufferImage = null; // bufferImage initialized separately and later
		graphics = null;	// same for graphics
	}
	
	@Override
	public void update(Graphics g) {
		
		paint(g);
	}
	/**
	 * Method to draw the buffer image on a graphics object that is
	 * obtained from the superclass. 
	 * Warning: do not override getGraphics() or drawing might fail. 
	 */
	public void update() {
		paint(getGraphics());
	}
	
	/**
	 * Draws the buffer image to the given graphics object.
	 * This method is called when this panel should redraw itself.
	 * The given graphics object is the one that actually shows 
	 * on the screen.
	 */
	@Override
	public void paint(Graphics g) {
		if (null == g) {
			System.out.println("MazePanel.paint: no graphics object, skipping drawImage operation. Or test.");
		}
		else {
			g.drawImage(bufferImage,0,0,null);	
		}
	}
	
	//panel.update
	/**
	 * Obtains a graphics object that can be used for drawing.
	 * This MazePanel object internally stores the graphics object 
	 * and will return the same graphics object over multiple method calls. 
	 * The graphics object acts like a notepad where all clients draw 
	 * on to store their contribution to the overall image that is to be
	 * delivered later.
	 * To make the drawing visible on screen, one needs to trigger 
	 * a call of the paint method, which happens 
	 * when calling the update method. 
	 * @return graphics object to draw on, null if impossible to obtain image
	 */
	public Graphics getBufferGraphics() {
		// if necessary instantiate and store a graphics object for later use
		if (null == graphics) { 
			if (null == bufferImage) {
				bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
				if (null == bufferImage)
				{
					System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
					return null; // still no buffer image, give up
				}
			}
			graphics = (Graphics2D) bufferImage.getGraphics();
			if (null == graphics) {
				System.out.println("Error: creation of graphics for buffered image failed, presumedly container not displayable");
			}
			else {
				// System.out.println("MazePanel: Using Rendering Hint");
				// For drawing in FirstPersonDrawer, setting rendering hint
				// became necessary when lines of polygons 
				// that were not horizontal or vertical looked ragged
				this.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
						RenderingHints.VALUE_ANTIALIAS_ON);
				this.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			}
		}
		return graphics;
	}
	
	@Override
	public void commit() {
		// TODO Auto-generated method stub
		paint(getGraphics());
	}
	
	@Override
	public boolean isOperational() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public void setColor(int rgb) {
		// TODO Auto-generated method stub
		if (graphics != null) {
			Color color = new Color(rgb);
			graphics.setColor(color);
		}
		int_col = rgb;
	}
	//SimpleScreens:can't get graphics object to draw on, skipping redraw operation
	
	/**
	 * New method for float colors!
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 */
	public void setColorFloat(float[] f) {
		if (graphics != null) {
			Color color = new Color(f[0],f[1],f[2],f[3]);
			graphics.setColor(color);
		}
	}
	

	private Color getCol() {
		return col;
	}


	@Override
	public int getColor() {
		// TODO Auto-generated method stub
		return int_col;
	}

	@Override
	public int getWallColor(int distance, int cc, int extensionX) {
		// TODO Auto-generated method stub
        // mod used to limit the number of colors to 6
        int d = distance;
        final int rgbValue = calculateRGBValue(d, extensionX);
        return rgbValue;
	}
	
	@Override
	public void addBackground(float percentToExit) {
		if (graphics != null) {
		// TODO Auto-generated method stub
		graphics.setColor(getBackgroundColor(percentToExit, true));
		graphics.fillRect(0, 0, getWidth(), getHeight()/2);
		// grey rectangle in lower half of screen
		// graphics.setColor(Color.darkGray);
		// dynamic color setting: 
		graphics.setColor(getBackgroundColor(percentToExit, false));
		graphics.fillRect(0, getHeight()/2, getWidth(), getHeight()/2);
	}
	}

	@Override
	public void addFilledRectangle(int x, int y, int width, int height) {
		if (graphics != null) {
		// TODO Auto-generated method stub
		graphics.fillRect(0, 0, width, height/2);
		}
	}

	@Override
	public void addFilledPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		// TODO Auto-generated method stub
		if (graphics != null) {
		graphics.fillPolygon(xPoints, yPoints, nPoints);
		}
	}

	@Override
	public void addPolygon(int[] xPoints, int[] yPoints, int nPoints) {
		// TODO Auto-generated method stub
		if (graphics != null) {
		graphics.drawPolyline(xPoints, yPoints, nPoints);
	}
	}

	@Override
	public void addLine(int startX, int startY, int endX, int endY) {
		// TODO Auto-generated method stub
		if (graphics != null) {
		graphics.drawLine(startX, startY, endX, endY);
		
	}
		}

	@Override
	public void addFilledOval(int x, int y, int w, int height) {
		// TODO Auto-generated method stub
		if (graphics != null) {
        graphics.fillOval(x, y, w, height);
	}
	}

	@Override
	public void addArc(int x, int y, int width, int height, int startAngle, int arcAngle) {
		if (graphics != null) {
		graphics.drawArc(x, y, width, height, startAngle, arcAngle);
		// TODO Auto-generated method stub
		}
	}

	@Override
	public void addMarker(float x, float y, String str) {
		// TODO Auto-generated method stub
		if (graphics != null){
		Font font = new Font(str, (int) x, (int) y);//fix
		graphics.setFont(font);
		}
	}

	@Override
	public void setRenderingHint(RenderingHints hintKey, RenderingHints hintValue) {
		Key key = null;
		Object object = null;
		/**
		 * Setting the key!
		 */
		if (hintKey == RenderingHints.KEY_ANTIALIASING) {
			key = java.awt.RenderingHints.KEY_ANTIALIASING;
		}
		else if (hintKey == RenderingHints.KEY_INTERPOLATION) {
			key = java.awt.RenderingHints.KEY_INTERPOLATION;
		}
		else if (hintKey == RenderingHints.KEY_RENDERING) {
			key = java.awt.RenderingHints.KEY_RENDERING;
		}
		/**
		 * Setting the value/object
		 */
		if (hintValue == RenderingHints.VALUE_ANTIALIAS_ON) {
			object = java.awt.RenderingHints.VALUE_ANTIALIAS_ON;
		}
		else if (hintValue == RenderingHints.VALUE_INTERPOLATION_BILINEAR) {
			object = java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR;
		}
		else if (hintValue == RenderingHints.VALUE_RENDER_QUALITY) {
			object = java.awt.RenderingHints.VALUE_RENDER_QUALITY;
		}
		graphics.setRenderingHint(key, object);
}
	
	/**
	 * GetbackgroundColor method from FirstPersonview
	 * @param percentToExit
	 * @param top
	 * @return
	 */
	private Color getBackgroundColor(float percentToExit, boolean top) {
		return top? blend(yellowWM, goldWM, percentToExit) : 
			blend(Color.lightGray, greenWM, percentToExit);
	}
	
	/**
	 * Color blend from FirstPersonView
	 * @param c0
	 * @param c1
	 * @param weight0
	 * @return
	 */
	private Color blend(Color c0, Color c1, double weight0) {
		if (weight0 < 0.1)
			return c1;
		if (weight0 > 0.95)
			return c0;
	    double r = weight0 * c0.getRed() + (1-weight0) * c1.getRed();
	    double g = weight0 * c0.getGreen() + (1-weight0) * c1.getGreen();
	    double b = weight0 * c0.getBlue() + (1-weight0) * c1.getBlue();
	    double a = Math.max(c0.getAlpha(), c1.getAlpha());

	    return new Color((int) r, (int) g, (int) b, (int) a);
	  }
	
	
    /**
     * From Wall!
     * Computes an RGB value based on the given numerical value.
     *
     * @param distance
     *            value to select color
     * @return the calculated RGB value
     */
    private int calculateRGBValue(final int distance, int extensionX) {
        // compute rgb value, depends on distance and x direction
        // 7 in binary is 0...0111
        // use AND to get last 3 digits of distance
        final int part1 = distance & 7;
        final int add = (extensionX != 0) ? 1 : 0;
        final int rgbValue = ((part1 + 2 + add) * 70) / 8 + 80;
        return rgbValue;
    }
    
    /**
     * stores fields into the given document with the help of MazeFileWriter.
     *
     * @param doc
     *            document to add data to
     * @param mazeXML
     *            element to add data to
     * @param number
     *            number for this element
     * @param i
     *            id for this element
     */
    public void storeWallPanel(final Document doc, final Element mazeXML,
            final int number, final int i) {
        MazeFileWriter.appendChild(doc, mazeXML, "colSeg_" + number + "_" + i,
                getCol().getRGB());
    }
    
 
    /**
     * Converts int array into normal integer!
     * Idea from https://www.shodor.org/stella2java/rgbint.html
     * @param red
     * @param blue
     * @param green
     * @return
     */
    public int colorSwitch(int red, int blue, int green) {
    	return 256*256*red+256*green+blue;
    }

	public void paintComponent(MazePanel panel) {
		// TODO Auto-generated method stubdfd
		
	}
    }
    

