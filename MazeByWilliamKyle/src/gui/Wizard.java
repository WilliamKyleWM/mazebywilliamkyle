/**
 * CRC
 * Class Wizard
 * Inherits from RobotDriver
 * Resonsibilities:
 * 				'telling' the robot what to do in case situations; finding the next closest cell to the exit, watching out for walls, etc.
 * Collaborates: 
 * 				with ReliableRobot, using its methods to guide the robot around the maze
 *
 */

package gui;

import java.util.HashMap;
import generation.CardinalDirection;
import generation.Distance;
import generation.Maze;
import gui.Robot.Direction;
import gui.Robot.Turn;
/**
 * @Author: WilliamKyle
 */
public class Wizard implements RobotDriver{
	private Robot robot;
	private Maze maze;
	private Distance distance;
	private ReliableSensor sensor;

	private DistanceMeth dist = new DistanceMeth();
	

	@Override
	public void setRobot(Robot r) {
		// TODO Auto-generated method stub
		this.robot = r;
		
	}
	/**
	 * The robot 'cheats' by using the setMaze method, 
	 * getting the info of the layout and distances
	 * Setmaze allows the algorithm to have and use the maze methods and variables.
	 */
	@Override
	public void setMaze(Maze maze) {
		// TODO Auto-generated method stub
		this.maze = maze;
	}
	/**
	 * Drives the robot one step towards the exit following
	 * its solution strategy and given the exists and  
	 * given the robot's energy supply lasts long enough. 
	 * @return true if driver successfully reaches the exit, false otherwise
	 * @throws Exception thrown if robot stopped due to some problem, e.g. lack of energy
	 */
	@Override
	public boolean drive2Exit() throws Exception {
		// TODO Auto-generated method stub
		HashMap<CardinalDirection, Integer> new_distances = new HashMap<>();
		for (CardinalDirection direction : CardinalDirection.values()) {
			new_distances.put(direction, Integer.MAX_VALUE);
		}
		
		while (!robot.hasStopped()) {
			int[] position = robot.getCurrentPosition();
			int current_distance = maze.getDistanceToExit(position[0], position[1]);
			
			isAtExit();
			
			CardinalDirection move_direction = null;
			for (CardinalDirection direction : CardinalDirection.values()) {
				int[] add = dist.directionConverter(direction);
				int[] new_position = new int[] {position[0] + add[0], position[1] + add[1]};
				if (new_position[0] >= 0 && new_position[0] <= maze.getWidth() - 1 && new_position[1] >= 0 && new_position[1] <= maze.getHeight() -1) {
					int distance = maze.getDistanceToExit(new_position[0], new_position[1]);
					
					if (distance == current_distance - 1) {
						CardinalDirection change = direction;
						//For some reason the North and South is swapped? Can't figure out why
						if (direction == CardinalDirection.North) {
							change = CardinalDirection.South;
						}
						else if (direction == CardinalDirection.South) {
							change = CardinalDirection.North;
						}
						if (!maze.hasWall(position[0], position[1], change)) {
							move_direction = direction;
					}
					}
					new_distances.replace(direction, distance);
				}
			}
			//ROTATE AND MOVE!
			
			CardinalDirection change = move_direction;
			//For some reason the North and South is swapped? Can't figure out why
			//This is to check if it will run into a wall!
			change = dist.directionSwitcher(move_direction);

			if (maze.hasWall(position[0], position[1], change)) {
				robot.setStopped(true);
			}
			
			robot.rotateTo(move_direction);
			robot.move(1);
		}
		System.out.println("FAILED FAILED");
		throw new Exception();
	}
	
	private boolean isAtExit() {
		DistanceMeth distance = new DistanceMeth();
		if (robot.isAtExit()) {
			//find direction of distance to infinity and turn towards it
			Turn last_one = Turn.AROUND;
			if (robot.canSeeThroughTheExitIntoEternity(Direction.FORWARD)){
					last_one = distance.directionToTurn(Direction.FORWARD);
			}
			else if (robot.canSeeThroughTheExitIntoEternity(Direction.LEFT)){
				last_one = distance.directionToTurn(Direction.LEFT);
			}
			else if (robot.canSeeThroughTheExitIntoEternity(Direction.RIGHT)){
				last_one = distance.directionToTurn(Direction.RIGHT);
			}
			if (last_one != Turn.AROUND) {
			robot.rotate(last_one);
			}
					robot.move(1);
					return true;
				}
		return false;
	}


	/**
	 * Drives the robot one step towards the exit following
	 * its solution strategy and given the exists and 
	 * given the robot's energy supply lasts long enough. 
	 * @return true if driver successfully performed one step, false otherwise
	 * @throws Exception thrown if robot stopped due to some problem, e.g. lack of energy
	 */
	@Override
	public boolean drive1Step2Exit() throws Exception {
		// TODO Auto-generated method stub
		HashMap<CardinalDirection, Integer> new_distances = new HashMap<>();
		for (CardinalDirection direction : CardinalDirection.values()) {
			new_distances.put(direction, Integer.MAX_VALUE);
		}
		
		if (!robot.hasStopped()) {
			int[] position = robot.getCurrentPosition();
			int current_distance = maze.getDistanceToExit(position[0], position[1]);
			
			isAtExit();
			
			CardinalDirection move_direction = null;
			for (CardinalDirection direction : CardinalDirection.values()) {
				int[] add = dist.directionConverter(direction);
				int[] new_position = new int[] {position[0] + add[0], position[1] + add[1]};
				if (new_position[0] >= 0 && new_position[0] <= maze.getWidth() - 1 && new_position[1] >= 0 && new_position[1] <= maze.getHeight() -1) {
					int distance = maze.getDistanceToExit(new_position[0], new_position[1]);
					
					if (distance == current_distance - 1) {
						CardinalDirection change = direction;
						//For some reason the North and South is swapped? Can't figure out why
						change = dist.directionSwitcher(direction);
						if (!maze.hasWall(position[0], position[1], change)) {
							move_direction = direction;
					}
					}
					new_distances.replace(direction, distance);
				}
			}
			//ROTATE AND MOVE!
			
			CardinalDirection change = move_direction;
			//For some reason the North and South is swapped? Can't figure out why
			//This is to check if it will run into a wall!

			change = dist.directionSwitcher(move_direction);
			
			if (maze.hasWall(position[0], position[1], change)) {
				robot.setStopped(true);
			}
			
			robot.rotateTo(move_direction);
			robot.move(1);
		}
		System.out.println("FAILED FAILED");
		throw new Exception();
	}


		// TODO Auto-generated method stub
		/*
		 * 	if robot is not out of power or crashed:
		 * 		if the robot is at the exit:
		 *(For special case at exit, don't want it measuring the infinite distance)
		 *
		 * 			if exit at north wall: 
		 * 				turn to the north
		 * 			if exit at east wall:
		 * 				turn to the east
		 * 			if exit at south wall:
		 * 				turn to the south
		 * 			if exit at west wall:
		 * 				turn to the west
		 * 
		 * 		move robot forward
		 * 		(woohoo exit)
		 * 		
		 * 	
		 * 		view the cells around the robots position
		 * 		if the getDistanceValue() is one less than the current distance value:
		 * 			if distanceToObstacle(direction) is not 0:
		 * 				turn robot towards that direction
		 * 		move robot forward
		 * 
		 * 	if out of power:
		 * 		robot fails :(
		 */
	

	/**
	 * returns the starting level (3500) minus the amount used!
	 */
	@Override
	public float getEnergyConsumption() {
		// TODO Auto-generated method stub
		return (EnergyCost.starting_level - robot.getBatteryLevel());
	}
	/**
	 * Returns odometer, very basic
	 */
	@Override
	public int getPathLength() {
		// TODO Auto-generated method stub
		return robot.getOdometerReading();
	}
	
	
	@Override
	public boolean drive2ExitEfficiently() throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

}