/**
 * 
 */
package gui;

import generation.Order;

import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JFrame;


/**
 * This class is a wrapper class to startup the Maze game as a Java application
 * 
 * This code is refactored code from Maze.java by Paul Falstad, www.falstad.com, Copyright (C) 1998, all rights reserved
 * Paul Falstad granted permission to modify and use code for teaching purposes.
 * Refactored by Peter Kemper
 * 
 * TODO: use logger for output instead of Sys.out
 */
public class MazeApplication extends JFrame {

	// not used, just to make the compiler, static code checker happy
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 */
	public MazeApplication() {
		init(null, null, null);
	}

	/**
	 * Constructor that loads a maze from a given file or uses a particular method to generate a maze
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
     * or a filename that stores an already generated maze that is then loaded, or can be null
	 */
	public MazeApplication(String generation, String driver, String sensors) {
		init(generation, driver, sensors);
	}

	/**
	 * Instantiates a controller with settings according to the given parameter.
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
	 * or a filename that contains a generated maze that is then loaded,
	 * or can be null
	 * @return the newly instantiated and configured controller
	 */
	 Controller createController(String generation, String driver, String sensors) {
	    // need to instantiate a controller to return as a result in any case
	    Controller result = new Controller() ;
	    // can decide if user repeatedly plays the same mazes or 
	    // if mazes are different each and every time
	    // set to true for testing purposes
	    // set to false for playing the game
	    result.setDeterministic(false);
	    String msg = null; // message for feedback
	    if ("WallFollower".equalsIgnoreCase(driver)) {
	    	msg = "Starting WallFollower algorithm";
	        RobotDriver drive = new WallFollower();
	        Robot robot = new UnReliableRobot();
	        //robot.setSensors(sensors);
	        result.setRobotAndDriverAndSensors(robot, drive, sensors);
	    }
	    else if ("Wizard".equalsIgnoreCase(driver)) {
	    	msg = "Starting Wizard algorithm";
	    	RobotDriver drive = new Wizard();
	    	Robot robot = new UnReliableRobot();
	    	//robot.setSensors(sensors);
	    	result.setRobotAndDriverAndSensors(robot,  drive, sensors);
	    }
	    else if ("WizardJump".equalsIgnoreCase(driver)) {
	    	msg = "Starting WizardJump algorithm";
	    	RobotDriver drive = new WizardJump();
	    	Robot robot = new UnReliableRobot();
	    	//robot.setSensors(sensors);
	    	result.setRobotAndDriverAndSensors(robot,  drive, sensors);
	    }
        else {
            // Not a driver
            msg = "MazeApplication: unknown parameter value: " + driver + " ignored, using wizard.";
	    	RobotDriver drive = new Wizard();
	    	Robot robot = new UnReliableRobot();
	    	//robot.setSensors(sensors);
	    	result.setRobotAndDriverAndSensors(robot,  drive, sensors);
        }
	    
	    
	    // Case 1 not needed anymore since generation is automatically set.
	    if ("DFS".equalsIgnoreCase(generation)) {
	        msg = "MazeApplication: maze will be generated with a randomized algorithm."; 
	    }
	    // Case 2: Prim
	    else if ("Prim".equalsIgnoreCase(generation))
	    {
	        msg = "MazeApplication: generating random maze with Prim's algorithm.";
	        result.setBuilder(Order.Builder.Prim);
	    }
	    // Case 3 a and b: Eller, Kruskal or some other generation algorithm
	    else if ("Kruskal".equalsIgnoreCase(generation))
	    {
	    	// TODO: for P2 assignment, please add code to set the builder accordingly
	        throw new RuntimeException("Don't know anybody named Kruskal ...");
	    }
	    else if ("Eller".equalsIgnoreCase(generation))
	    {
	    	// TODO: for P2 assignment, please add code to set the builder accordingly
	    	msg = "MazeApplication: generating random maze with Eller's algorithm.";
	    	result.setBuilder(Order.Builder.Eller);
	    }
	    // Case 4: a file
	    else {
	        File f = new File(generation);
	        if (f.exists() && f.canRead())
	        {
	            msg = "MazeApplication: loading maze from file: " + generation;
	            result.setFileName(generation);
	            return result;
	        }
	        else {
	            // None of the predefined strings and not a filename either: 
	            msg = "MazeApplication: unknown parameter value: " + generation + " ignored, operating in default mode.";
	            
	        }
	    }
	    // controller instanted and attributes set according to given input parameter
	    // output message and return controller
	    System.out.println(msg);
	    return result;
	}

	/**
	 * Initializes some internals and puts the game on display.
	 * @param parameter can identify a generation method (Prim, Kruskal, Eller)
     * or a filename that contains a generated maze that is then loaded, or can be null
	 * @param sensors 
	 * @param driver 
	 */
	private void init(String generation, String driver, String sensors) {
	    // instantiate a game controller and add it to the JFrame
	    Controller controller = createController(generation, driver, sensors);
		add(controller.getPanel()) ;
		// instantiate a key listener that feeds keyboard input into the controller
		// and add it to the JFrame
		KeyListener kl = new SimpleKeyListener(this, controller) ;
		addKeyListener(kl) ;
		// set the frame to a fixed size for its width and height and put it on display
		setSize(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT+22) ;
		setVisible(true) ;
		// focus should be on the JFrame of the MazeApplication and not on the maze panel
		// such that the SimpleKeyListener kl is used
		setFocusable(true) ;
		// start the game, hand over control to the game controller
		controller.start();
	}
	
	/**
	 * Main method to launch Maze game as a java application.
	 * The application can be operated in three ways. 
	 * 1) The intended normal operation is to provide no parameters
	 * and the maze will be generated by a randomized DFS algorithm (default). 
	 * 2) If a filename is given that contains a maze stored in xml format. 
	 * The maze will be loaded from that file. 
	 * This option is useful during development to test with a particular maze.
	 * 3) A predefined constant string is given to select a maze
	 * generation algorithm, currently supported is "Prim".
	 * @param args is optional, first string can be a fixed constant like Prim or
	 * the name of a file that stores a maze in XML format
	 */
	public static void main(String[] args) {
	    JFrame app ; 
	    /**
	    *http://journals.ecs.soton.ac.uk/java/tutorial/java/cmdLineArgs/parsing.html
	    * Credit to The Java Tutorial for the command line parser.
	    */
        int i = 0;
        String arg;
        boolean vflag = true;
        String generation = "DFS";
        String driver = "Wizard";
        String sensors = "1111";

        while (i < args.length && args[i].startsWith("-")) {
            arg = args[i++];


    // Maze Generation algorithm
            if (arg.equals("-g")) {
                if (i < args.length)
                    generation = args[i++];
                else
                    System.err.println("-output requires an argument such as 'Eller'");
                if (vflag)
                    System.out.println("generation = " + generation);
            }
            //Robot Driver
            else if (arg.equals("-d")) {
                if (i < args.length)
                    driver = args[i++];
                else
                    System.err.println("-output requires an argument such as 'Wizard'");
                if (vflag)
                    System.out.println("driver = " + driver);
            }
            // Sensor specification
            else if (arg.equals("-r")) {
                if (i < args.length)
                    sensors = args[i++];
                else
                    System.err.println("-r requires an argument such as '0101'");
                if (vflag)
                    System.out.println("sensor mode = " + sensors);
            }
        }
	    
	    
		switch (args.length) {
		case 3: app = new MazeApplication(generation, driver, sensors);
		break;
		case 2: app = new MazeApplication(generation, driver, sensors);
		break;
		case 1 : app = new MazeApplication(generation, driver, sensors);
		break ;
		case 0 : app = new MazeApplication(generation, driver, sensors);
		break;
		default : app = new MazeApplication(generation, driver, sensors) ;
		break ;
		}
		app.repaint() ;
	}
}
