/**
 * CRC
 * Class ReliableRobot
 * Inherits from Robot
 * Resonsibilities:
 * 				moving, rotating, and jumping (not yet) the robot around the maze.
 * Collaborates: 
 * 				with ReliableSensor to find where the exit is (sensing distances to objects and the like)
 * 				with Wizard, getting directions from the algorithm and moving the robot around
 */

package gui;

import java.util.Arrays;

import java.util.List;

import generation.CardinalDirection;
import generation.Floorplan;
import generation.Maze;
import gui.Constants.UserInput;
import gui.EnergyCost;

/**
 * @Author: WilliamKyle
 */
public class ReliableRobot implements Robot{
	private Controller control;
	private float battery_level;
	private int odometer;
	private float start_battery = EnergyCost.starting_level;
	private int[] position;
	boolean stopped;
	private Maze maze;
	DistanceSensor sensor;
	private int[] sensors = {1,1,1,1};
	private DistanceMeth dist;
	
	//initialize
	public ReliableRobot() {
		sensor = new ReliableSensor();
		sensor.setMaze(maze);
		battery_level = start_battery;
		odometer = 0;
		stopped = false;
		//control.getMazeConfiguration();
		
	}
	
	
	@Override
	public void setController(Controller controller) {
		// TODO Auto-generated method stub
		this.control = controller;
		update_position();
	}
	
	@Override
	public void setMaze(Maze maze) {
		this.maze = maze;
	}

	@Override
	public int[] getCurrentPosition() throws Exception {
		// TODO Auto-generated method stub
		int[] position = control.getCurrentPosition();
		return position;
	}

	@Override
	public CardinalDirection getCurrentDirection() {
		// TODO Auto-generated method stub
		CardinalDirection direction = control.getCurrentDirection();
		return direction;
	}

	@Override
	public float getBatteryLevel() {
		// TODO Auto-generated method stub
		return battery_level;
	}

	@Override
	public void setBatteryLevel(float level) {
		// TODO Auto-generated method stub
		battery_level = level;
	}

	@Override
	public float getEnergyForFullRotation() {
		// TODO Auto-generated method stub
		return EnergyCost.rotate*4;
	}

	@Override
	public float getEnergyForStepForward() {
		// TODO Auto-generated method stub
		return EnergyCost.forward;
	}

	@Override
	public int getOdometerReading() {
		// TODO Auto-generated method stub
		return odometer;
	}

	@Override
	public void resetOdometer() {
		// TODO Auto-generated method stub
		odometer = 0;
	}

	@Override
	public void rotate(Turn turn) {
		// TODO Auto-generated method stub
		if (battery_level >= EnergyCost.rotate) {
			if (turn == Robot.Turn.LEFT) {
				control.keyDown(UserInput.Left, 0);
				battery_level -= EnergyCost.rotate;
			}
			else if (turn == Robot.Turn.RIGHT) {
				control.keyDown(UserInput.Right, 0);
				battery_level -= EnergyCost.rotate;
			}
			else if (turn == Robot.Turn.AROUND) {
				if (battery_level >= EnergyCost.rotate*2) {
					control.keyDown(UserInput.Right,0);
					control.keyDown(UserInput.Right,0);
					battery_level -= EnergyCost.rotate*2;
				}
				else {
					stopped = true;
				}
			}
		}
		else {
			stopped = true;
		}
	}
	/**
	 * Move method uses the distance parameter to move the robot that distance forward, checkign battery_level
	 * and if there is a wallboard in the way.
	 */
	@Override
	public void move(int distance) {
		maze = control.getMazeConfiguration();
		CardinalDirection cur_direction = getCurrentDirection();
		update_position();
		if (distance >0) {
			int forward = 0;
			while (forward < distance) {
				if (battery_level >= EnergyCost.forward) {
					if (!maze.hasWall(position[0], position[1], cur_direction)) {
						control.keyDown(UserInput.Up, 0);
						update_position();
						battery_level -= getEnergyForStepForward();
						odometer += 1;
						forward += 1;
						
					}
					else {
						stopped = true;
					}
				}
			
			else {
				stopped = true;
				break;
			}
			}
		}
	}

	/**
	 * Jump method; checks that it's not at the maze edge and then makes the jump, otherwise, the robot stops
	 * 
	 */
	@Override
	public void jump() throws Exception {
		// TODO Auto-generated method stub
		CardinalDirection cur_direction = getCurrentDirection();
		update_position();
		//Catch that the jump is not over the outside wallboard!
		if (cur_direction == CardinalDirection.North) {
			if (position[1]==0) {
				stopped = true;
				throw new Exception("Jumping outside the maze");
			}
		}
		else if (cur_direction == CardinalDirection.East) {
			if (position[1]==0) {
				stopped = true;
				throw new Exception("Jumping outside the maze");
		}
		}
		else if (cur_direction == CardinalDirection.South) {
			if (position[1]==0) {
				stopped = true;
				throw new Exception("Jumping outside the maze");
		}
		}
		else if (cur_direction == CardinalDirection.West) {
			if (position[1]==0) {
				stopped = true;
				throw new Exception("Jumping outside the maze");
		}
		}
		if (battery_level >= EnergyCost.jump) {
			control.keyDown(UserInput.Jump, 0);
			battery_level -= EnergyCost.jump;
			System.out.println("JUMP");
			odometer += 1;
		}
		}
		

	@Override
	public boolean isAtExit() {
		// TODO Auto-generated method stub
		Maze maze = control.getMazeConfiguration();
		try {
			position = getCurrentPosition();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean at_exit;
		if (maze.getDistanceToExit(position[0],position[1]) == 1) {
			at_exit = true;
		}
		else {
			at_exit = false;
		}
		return at_exit;
	}

	@Override
	public boolean isInsideRoom() {
		// TODO Auto-generated method stub
		Maze maze = control.getMazeConfiguration();
		update_position();
		return maze.getFloorplan().isInRoom(position[0], position[1]);
	}
	@Override
	public void setStopped(boolean stopped) {
		this.stopped = stopped;
	}
	@Override
	public boolean hasStopped() {
		// TODO Auto-generated method stub
		return stopped;
	}

	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		battery_level -= EnergyCost.dist_sense;
		// TODO Auto-generated method stub
		CardinalDirection direct = dist.absoluteDirection(this, direction);
		int[] pos = null;
		try {
			pos = this.getCurrentPosition();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int obstacle_distance = -1;
		sensor.setMaze(maze);
		try {
			obstacle_distance = sensor.distanceToObstacle(pos, direct, battery_level);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obstacle_distance;
	}
			
		/*
		 * if enough battery for dist_sense:
		 * 		start_direct = getCurrentDirection()
		 * 		start_position = getCurrentPosition()
		 * 		while getCurrentDirection() is not direction:
		 * 			rotate right
		 * 		counter = 0
		 * 	
		 * 		while no wallboard at start_position() :
		 * 			move(1)
		 * 			counter +=1
		 * 		turn around
		 * 		move(counter)
		 * 		return counter
		 * 			
		 * 		
		 * 		
		 */


	@Override
	public boolean canSeeThroughTheExitIntoEternity(Direction direction) throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		// great method name lol
		return distanceToObstacle(direction) == Integer.MAX_VALUE;
	}

	@Override
	public void startFailureAndRepairProcess(Direction direction, int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		//optional operation
		
	}

	@Override
	public void stopFailureAndRepairProcess(Direction direction) throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		// optional operation
		
	}
	



	@Override
	public void rotateTo(CardinalDirection start_direct) {
		if (battery_level>=EnergyCost.rotate) {
			
		
		CardinalDirection direction;
		direction = control.getCurrentDirection();
		if (direction == CardinalDirection.South) {
			direction = CardinalDirection.North;
		}
		else if (direction == CardinalDirection.North) {
			direction = CardinalDirection.South;
		}
		//Forwards
		Turn turn = null;
		turn = dist.directionSelectorCardinal(start_direct, direction);
		if (turn != null) {
			rotate(turn);
		}
				
	}
		else {
			this.setStopped(true);
		}
	}



	/**
	 * Updates the position!!! Very straightforward. I kept having crashes and this seemed to solve the crashing.
	 */
	private void update_position() {
		try {
			position = getCurrentPosition();
		} catch (Exception e) {
		}
	}


	@Override
	public void setSensors(String sensors) {
		// TODO Auto-generated method stub
		//not required for ReliableRobot
		
	}


	@Override
	public boolean sensorWorks(Direction direction) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void sensorFail(Direction direction) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void sensorFix(Direction direction) {
		// TODO Auto-generated method stub
		
	}


	
	//directionSelector failed

}
