package gui;

/**
 * 
 * @author: WilliamKyle
 *...
 *@whatitsFor This class holds the movement concepts 
 *detailed in the energy consumption section
 *of the project 3 pdf
 */
public class EnergyCost {
	public static int dist_sense = 1;
	public static int rotate = 3;
	public static int forward = 6;
	public static int jump = 40;
	public static float starting_level = 3500;

}
