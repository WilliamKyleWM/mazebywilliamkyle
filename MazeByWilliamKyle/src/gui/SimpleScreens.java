package gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
/**
 * Implements the screens that are displayed whenever the game is not in 
 * the playing state. The screens shown are the title screen, 
 * the generating screen with the progress bar during maze generation,
 * and the final screen when the game finishes.
 * The only one that is not simple and not covered by this class
 * is the one that shows the first person view of the maze game
 * and the map of the maze when the user really navigates inside the maze.
 * 
 * W&M specific color settings
 * Green: #115740
 * Gold: #916f41
 * Black: #222222
 * 
 * Design: 
 * white background with green and gold frame 
 * title text large and gold, 
 * normal text small and green
 * small text small and black
 * 
 * @author Peter Kemper
 *
 */
public class SimpleScreens {
	// Color settings
	final Color will_color1 = Color.black;
	final Color will_color2 = Color.red;
	final Color will_color3 = Color.getHSBColor(60, 100, 100);
	final String errorMsg = "SimpleScreens:can't get graphics object to draw on, skipping redraw operation";
	/**
	 * Test variable
	 */
	    
    /**
     * Draws the title screen, screen content is hard coded
     * @param panel holds the graphics for the off-screen image
     * @param filename is a string put on display for the file
     * that contains the maze, can be null
     */
    public void redrawTitle(MazePanel panel, String filename) {
    	Graphics g = panel.getBufferGraphics() ;
        if (null == g) {
            System.out.println(errorMsg) ;
        }
        else {
            redrawTitle(g,filename);
        }
    }
    /**
     * Helper method for redraw to draw the title screen, screen is hard coded
     * @param  gc graphics is the off-screen image, can not be null
     * @param filename is a string put on display for the file
     * that contains the maze, can be null
     */
    private void redrawTitle(Graphics gc, String filename) {
        // produce white background
    	drawBackground(gc);
        // write the title 
    	updateFontAndColor(gc, largeBannerFont, will_color1);
        centerString(gc, "WILL's MAZE", 98);
        updateFontAndColor(gc, largeBannerFont, will_color2);
        centerString(gc, "WILL's MAZE", 100);
        // write the reference to Paul Falstad
        updateFontAndColor(gc, smallBannerFont, will_color1);
        centerString(gc, "by Paul Falstad", 160);
        centerString(gc, "www.falstad.com", 190);
        // write the instructions in black, same smallBannerFont as before
        if (filename == null) {
        	// default instructions
        	gc.setColor(will_color1);
        	centerString(gc, "To start, select a skill level.", 252);
        	centerString(gc, "(Press a number from 0 to 9,", 302);
        	centerString(gc, "or a letter from a to f)", 322);
        	gc.setColor(will_color3);
        	centerString(gc, "To start, select a skill level.", 250);
        	centerString(gc, "(Press a number from 0 to 9,", 300);
        	centerString(gc, "or a letter from a to f)", 320);
        }
        else {
        	gc.setColor(will_color1);
        	centerString(gc, "Loading maze from file:", 252);
        	centerString(gc, filename, 302);
        	gc.setColor(will_color3);
        	// message if maze is loaded from file
        	centerString(gc, "Loading maze from file:", 250);
        	centerString(gc, filename, 300);
        }
        gc.setColor(will_color1);
        centerString(gc, "Version 4.1", 352);
        gc.setColor(will_color3);
        centerString(gc, "Version 4.1", 350);
    }
    /**
     * Updates the font and color settings of the given graphics object
     * @param gc
     * @param font
     * @param color
     */
	private void updateFontAndColor(Graphics gc, Font font, Color color) {
		gc.setFont(font);
        gc.setColor(color);
	}
    /**
     * Draws the background, a green and cold frame with
     * a white center stage area
     * @param gc the graphics to draw on
     */
	private void drawBackground(Graphics gc) {
		gc.setColor(will_color1);
        gc.fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
        gc.setColor(will_color2);
        gc.fillRect(10, 10, Constants.VIEW_WIDTH-20, Constants.VIEW_HEIGHT-20);
        gc.setColor(Color.gray);
        gc.fillRect(15, 15, Constants.VIEW_WIDTH-30, Constants.VIEW_HEIGHT-30);
	}
	

	private void dbg(String str) {
		System.out.println("MazeView:" + str);
	}
    /**
     * Draws the finish screen, screen content is hard coded
     * @param panel holds the graphics for the off-screen image
     */
	void redrawFinish(MazePanel panel, float battery_level, boolean stopped, int odometer) {
		Graphics g = panel.getBufferGraphics() ;
        if (null == g) {
            System.out.println(errorMsg) ;
        	
        }
        else {
            redrawFinish(g, battery_level, stopped, odometer);
        }
	}
	/**
	 * Helper method for redraw to draw final screen, screen is hard coded
	 * @param gc graphics is the off-screen image
	 */
	private void redrawFinish(Graphics gc, float battery_level, boolean stopped, int odometer) {
		// produce blue background
		drawBackground(gc);
		// write the title 
		updateFontAndColor(gc, largeBannerFont, will_color2);
		if (stopped) {
			centerString(gc, "You Lost!", 100);
			updateFontAndColor(gc, smallBannerFont, will_color2);
			centerString(gc, "Out of Battery or crashed", 250);
		}
		else {
		centerString(gc, "You won!", 100);
		}
		// write some extra blurb
		updateFontAndColor(gc, smallBannerFont, will_color1);
		String b = String.valueOf(battery_level);
		String o = String.valueOf(odometer);
		centerString(gc, "Battery Level "+b, 160);
		centerString(gc ,"Odometer "+o, 190);
		// write the instructions
		gc.setColor(will_color3);
		centerString(gc, "Hit any key to restart", 300);
	}
    /**
     * Draws the generating screen, screen content is hard coded
     * @param panel holds the graphics for the off-screen image
     */
    public void redrawGenerating(MazePanel panel, int percentDone) {
    	Graphics g = panel.getBufferGraphics() ;
        if (null == g) {
            System.out.println(errorMsg) ;
        }
        else {
            redrawGenerating(g, percentDone);
        }
    }
	/**
	 * Helper method for redraw to draw screen during phase of maze generation,
	 * screen is hard coded, only percentage is dynamic
	 * @param gc graphics is the off screen image
	 * @param percentage is the percentage of progress to show
	 */
	private void redrawGenerating(Graphics gc, int percentage) {
		// produce  background and  title
		drawBackground(gc);
		updateFontAndColor(gc, largeBannerFont, will_color2);
		centerString(gc, "Building maze", 150);
		// show progress
		updateFontAndColor(gc, smallBannerFont, will_color1);
		centerString(gc, percentage + "% completed", 200);
		// write the instructions
		gc.setColor(will_color3);
		centerString(gc, "Hit escape to stop", 300);
	}
	
	private void centerString(Graphics g, String str, int ypos) {
		g.drawString(str, 
				(Constants.VIEW_WIDTH-g.getFontMetrics().stringWidth(str))/2, 
				ypos);
	}

	final Font largeBannerFont = new Font("TimesRoman", Font.BOLD, 48);
	final Font smallBannerFont = new Font("TimesRoman", Font.BOLD, 16);

}
