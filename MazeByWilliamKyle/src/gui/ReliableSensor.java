/**
 * CRC
 * Class ReliableSensor
 * Inherits from DistanceSensor
 * Resonsibilities:
 * 				moving, rotating, and jumping (not yet) the robot around the maze.
 * Collaborates: 
 * 				with ReliableRobot to sense the distance from obstacles
 *
 */

package gui;

import java.util.Arrays;
import java.util.List;

import generation.CardinalDirection;
import generation.Maze;
import gui.Robot.Direction;

/**
 * @Author: WilliamKyle
 */
public class ReliableSensor implements DistanceSensor{
	Controller control;
	Maze maze;	
	int[] sensors; // probably will use this for sensors breaking down eventually? Right now pointless.
	
	public ReliableSensor() {
	}
	
	@Override
	public int distanceToObstacle(int[] currentPosition, CardinalDirection currentDirection, float powersupply)
			throws Exception {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		if (powersupply >= EnergyCost.dist_sense) {
			int[] start_position = currentPosition;
						//Finding absolute direction
			CardinalDirection[] hold = {CardinalDirection.North, CardinalDirection.East, CardinalDirection.South, CardinalDirection.West};
			List<CardinalDirection> directions = Arrays.asList(hold);
			
			int count = 0;
			if (currentDirection == CardinalDirection.North) {
				currentDirection = CardinalDirection.South;
			}
			else if (currentDirection == CardinalDirection.South) {
				currentDirection = CardinalDirection.North;
			}
			int[] approach = directionConverter(currentDirection);
			if (currentDirection == CardinalDirection.North) {
				currentDirection = CardinalDirection.South;
			}
			else if (currentDirection == CardinalDirection.South) {
				currentDirection = CardinalDirection.North;
			}
			while (!maze.hasWall(start_position[0], start_position[1], currentDirection)) {
				currentPosition[0] += approach[0];
				currentPosition[1] += approach[1];
				
				if (!maze.isValidPosition(currentPosition[0], currentPosition[1])) {
					return Integer.MAX_VALUE;
				}
				count +=1;
				
							}
			return count;
		}
			else {
				control.robot.setStopped(true);
				return -1;
			}
	}
		

	@Override
	public void setMaze(Maze maze) {
		// TODO Auto-generated method stub
		this.maze = maze;
	}

	@Override
	public void setSensorDirection(Direction mountedDirection) {
		if (mountedDirection == null) {
			throw new IllegalArgumentException();
		}
	}

	@Override
	public float getEnergyConsumptionForSensing() {
		// TODO Auto-generated method stub
		return EnergyCost.dist_sense;
	}

	@Override
	public void startFailureAndRepairProcess(int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stopFailureAndRepairProcess() throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		
	}

/**
 * Copied from ReliableRobot over.
 * @param direction
 * @return
 */
public static int[] directionConverter(CardinalDirection direction) {
	if (direction==CardinalDirection.North) {
			return new int[] {0, 1};
	}
	else if (direction==CardinalDirection.South) {
		return new int[] {0, -1};
}
	else if (direction==CardinalDirection.East) {
		return new int[] {1, 0};
}
	else if (direction==CardinalDirection.West) {
		return new int[] {-1, 0};
}
	else{
			return null;
	}
}
}
