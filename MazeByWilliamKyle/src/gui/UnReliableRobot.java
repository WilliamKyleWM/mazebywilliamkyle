/**
 * CRC
 * Class ReliableRobot
 * Inherits from Robot
 * Resonsibilities:
 * 				moving, rotating, and jumping (not yet) the robot around the maze.
 * Collaborates: 
 * 				with ReliableSensor to find where the exit is (sensing distances to objects and the like)
 * 				with Wizard, getting directions from the algorithm and moving the robot around
 */

package gui;

import java.util.Arrays;

import java.util.List;

import generation.CardinalDirection;
import generation.Floorplan;
import generation.Maze;
import gui.Constants.UserInput;
import gui.Robot.Turn;
import gui.EnergyCost;

/**
 * @Author: WilliamKyle
 */
public class UnReliableRobot implements Robot{
	private Controller control;
	private float battery_level;
	private int odometer;
	private float start_battery = EnergyCost.starting_level;
	private int[] position;
	boolean stopped;
	private Maze maze;
	private DistanceSensor sensor_front;
	private DistanceSensor sensor_left;
	private DistanceSensor sensor_right;
	private DistanceSensor sensor_back;
	boolean[] sensors;
	boolean[] sensor_array = new boolean[] {true,true,true,true};
	private DistanceMeth dist = new DistanceMeth();
	
	//initialize
	public UnReliableRobot() {
		
		
		battery_level = start_battery;
		odometer = 0;
		stopped = false;
		
	}
	
	
	@Override
	public void setController(Controller controller) {
		// TODO Auto-generated method stub
		this.control = controller;
		update_position();
	}
	
	@Override
	public void setMaze(Maze maze) {
		this.maze = maze;
	}

	@Override
	public int[] getCurrentPosition() throws Exception {
		// TODO Auto-generated method stub
		int[] position = control.getCurrentPosition();
		return position;
	}

	@Override
	public CardinalDirection getCurrentDirection() {
		// TODO Auto-generated method stub
		CardinalDirection direction = control.getCurrentDirection();
		return direction;
	}

	@Override
	public float getBatteryLevel() {
		// TODO Auto-generated method stub
		return battery_level;
	}

	@Override
	public void setBatteryLevel(float level) {
		// TODO Auto-generated method stub
		battery_level = level;
	}

	@Override
	public float getEnergyForFullRotation() {
		// TODO Auto-generated method stub
		return EnergyCost.rotate*4;
	}

	@Override
	public float getEnergyForStepForward() {
		// TODO Auto-generated method stub
		return EnergyCost.forward;
	}

	@Override
	public int getOdometerReading() {
		// TODO Auto-generated method stub
		return odometer;
	}

	@Override
	public void resetOdometer() {
		// TODO Auto-generated method stub
		odometer = 0;
	}

	@Override
	public void rotate(Turn turn) {
		// TODO Auto-generated method stub
		if (battery_level >= EnergyCost.rotate) {
			if (turn == Robot.Turn.LEFT) {
				control.keyDown(UserInput.Left, 0);
				battery_level -= EnergyCost.rotate;
			}
			else if (turn == Robot.Turn.RIGHT) {
				control.keyDown(UserInput.Right, 0);
				battery_level -= EnergyCost.rotate;
			}
			else if (turn == Robot.Turn.AROUND) {
				if (battery_level >= EnergyCost.rotate*2) {
					control.keyDown(UserInput.Right,0);
					control.keyDown(UserInput.Right,0);
					battery_level -= EnergyCost.rotate*2;
				}
				else {
					stopped = true;
				}
			}
		}
		else {
			stopped = true;
		}
	}
	/**
	 * Move method uses the distance parameter to move the robot that distance forward, checkign battery_level
	 * and if there is a wallboard in the way.
	 */
	@Override
	public void move(int distance) {
		maze = control.getMazeConfiguration();
		CardinalDirection cur_direction = getCurrentDirection();
		update_position();
		if (distance >0) {
			int forward = 0;
			while (forward < distance) {
				if (battery_level >= EnergyCost.forward) {
					if (!maze.hasWall(position[0], position[1], cur_direction)) {
						control.keyDown(UserInput.Up, 0);
						update_position();
						battery_level -= getEnergyForStepForward();
						odometer += 1;
						forward += 1;
						
					}
					else {
						stopped = true;
					}
				}
			
			else {
				stopped = true;
				break;
			}
			}
		}
	}

	/**
	 * Jump method; checks that it's not at the maze edge and then makes the jump, otherwise, the robot stops
	 * 
	 */
	@Override
	public void jump() throws Exception {
		// TODO Auto-generated method stub
		maze = control.getMazeConfiguration();
		CardinalDirection cur_direction = getCurrentDirection();
		int[] position = this.getCurrentPosition();
		//Catch that the jump is not over the outside wallboard!
		if (cur_direction == CardinalDirection.North) {
			if (position[1]==0) {
				stopped = true;
				throw new Exception("Jumping outside the maze");
			}
		}
		else if (cur_direction == CardinalDirection.East) {
			if (position[0]==maze.getWidth()-1) {
				stopped = true;
				throw new Exception("Jumping outside the maze");
		}
		}
		else if (cur_direction == CardinalDirection.South) {
			if (position[0]==maze.getHeight()-1) {
				stopped = true;
				throw new Exception("Jumping outside the maze");
		}
		}
		else if (cur_direction == CardinalDirection.West) {
			if (position[0]==0) {
				stopped = true;
				throw new Exception("Jumping outside the maze");
		}
		}
		if (battery_level >= EnergyCost.jump) {
			control.keyDown(UserInput.Jump, 0);
			battery_level -= EnergyCost.jump;
			odometer += 1;
		}
		}
		

	@Override
	public boolean isAtExit() {
		// TODO Auto-generated method stub
		Maze maze = control.getMazeConfiguration();
		try {
			position = getCurrentPosition();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean at_exit;
		if (maze.getDistanceToExit(position[0],position[1]) == 1) {
			at_exit = true;
		}
		else {
			at_exit = false;
		}
		return at_exit;
	}

	@Override
	public boolean isInsideRoom() {
		// TODO Auto-generated method stub
		Maze maze = control.getMazeConfiguration();
		update_position();
		return maze.getFloorplan().isInRoom(position[0], position[1]);
	}
	@Override
	public void setStopped(boolean stopped) {
		this.stopped = stopped;
	}
	@Override
	public boolean hasStopped() {
		// TODO Auto-generated method stub
		return stopped;
	}
	

	
	public Direction directionSelectorCardinal(CardinalDirection start_direct, CardinalDirection direction) {
		//Forwards
		if (((start_direct == direction))) {
			return Robot.Direction.FORWARD;
		}
		//Backwards
		else if (((start_direct == CardinalDirection.North) && (direction == CardinalDirection.South)) || ((start_direct == CardinalDirection.South) &&(direction == CardinalDirection.North))){
			return Robot.Direction.BACKWARD;
		}
		else if (((start_direct == CardinalDirection.West) && (direction == CardinalDirection.East)) || ((start_direct == CardinalDirection.East) &&(direction == CardinalDirection.West))){
			return Robot.Direction.BACKWARD;
		}
		//Right
		else if (((start_direct == CardinalDirection.South) && (direction == CardinalDirection.West)) || ((start_direct == CardinalDirection.West) &&(direction == CardinalDirection.North))){
			return Robot.Direction.RIGHT;
		}
		else if (((start_direct == CardinalDirection.North) && (direction == CardinalDirection.East)) || ((start_direct == CardinalDirection.East) &&(direction == CardinalDirection.South))){
			return Robot.Direction.RIGHT;
		}
		//Left
		else if (((start_direct == CardinalDirection.East) && (direction == CardinalDirection.North)) || ((start_direct == CardinalDirection.North) &&(direction == CardinalDirection.West))){
			return Robot.Direction.LEFT;
		}
		else if (((start_direct == CardinalDirection.West) && (direction == CardinalDirection.South)) || ((start_direct == CardinalDirection.South) &&(direction == CardinalDirection.East))){
			return Robot.Direction.LEFT;
		}
		else {
			System.out.println("directionSelector failed!");
			return null;
			
		}
	}

	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
		battery_level -= EnergyCost.dist_sense;
		// TODO Auto-generated method stub
		CardinalDirection direct = dist.absoluteDirection(this,direction);
		int[] pos = null;
		try {
			pos = this.getCurrentPosition();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int obstacle_distance = -1;
		//sensor.setMaze(maze);
		sensor_left.setMaze(maze);
		sensor_right.setMaze(maze);
		sensor_back.setMaze(maze);
		sensor_front.setMaze(maze);
		if (direction == Direction.BACKWARD && (sensor_array[2])) {
			try {
				obstacle_distance = sensor_back.distanceToObstacle(pos, direct, battery_level);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (direction == Direction.FORWARD && (sensor_array[0])) {
			try {
				obstacle_distance = sensor_front.distanceToObstacle(pos, direct, battery_level);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (direction == Direction.LEFT && (sensor_array[1])) {
			try {
				obstacle_distance = sensor_left.distanceToObstacle(pos, direct, battery_level);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if (direction == Direction.RIGHT && (sensor_array[3])) {
			try {
				obstacle_distance = sensor_right.distanceToObstacle(pos, direct, battery_level);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return obstacle_distance;
	}
			
		/*
		 * if enough battery for dist_sense:
		 * 		start_direct = getCurrentDirection()
		 * 		start_position = getCurrentPosition()
		 * 		while getCurrentDirection() is not direction:
		 * 			rotate right
		 * 		counter = 0
		 * 	
		 * 		while no wallboard at start_position() :
		 * 			move(1)
		 * 			counter +=1
		 * 		turn around
		 * 		move(counter)
		 * 		return counter
		 * 			
		 * 		
		 * 		
		 */


	@Override
	public boolean canSeeThroughTheExitIntoEternity(Direction direction) throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		// great method name lol
		return distanceToObstacle(direction) == Integer.MAX_VALUE;
	}

	@Override
	public void startFailureAndRepairProcess(Direction direction, int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		//optional operation
		
	}

	@Override
	public void stopFailureAndRepairProcess(Direction direction) throws UnsupportedOperationException {
		// TODO Auto-generated method stub
		// optional operation
	}
	
	@Override
	public void sensorFail(Direction direction) {
		if (direction == Direction.FORWARD) {
			sensor_array[0] = false;
		}
		else if (direction == Direction.LEFT) {
			sensor_array[1] = false;
		}
		else if (direction == Direction.BACKWARD) {
			sensor_array[2] = false;
		}
		else if (direction == Direction.RIGHT) {
			sensor_array[3] = false;
		}
	}
	
	@Override
	public void sensorFix(Direction direction) {
		if (direction == Direction.FORWARD) {
			sensor_array[0] = true;
		}
		else if (direction == Direction.LEFT) {
			sensor_array[1] = true;
		}
		else if (direction == Direction.BACKWARD) {
			sensor_array[2] = true;
		}
		else if (direction == Direction.RIGHT) {
			sensor_array[3] = true;
		}
	}
	
	@Override
	public boolean sensorWorks(Direction direction) {
		if (direction == Direction.FORWARD) {
			return sensor_array[0];
		}
		else if (direction == Direction.LEFT) {
			return sensor_array[1];
		}
		else if (direction == Direction.BACKWARD) {
			return sensor_array[2];
		}
		else if (direction == Direction.RIGHT) {
			return sensor_array[3];
		}
		else {
			return false;
		}
	}


	@Override
	public void rotateTo(CardinalDirection start_direct) {
		if (battery_level>=EnergyCost.rotate) {
			
		
		CardinalDirection direction;
		direction = control.getCurrentDirection();
		direction = dist.directionSwitcher(direction);
		//Forwards
		Turn turn = null;
		turn = dist.directionSelectorCardinal(start_direct, direction);
		if (turn != null) {
			rotate(turn);
		}

	}
		else {
			this.setStopped(true);
		}
	}


	/**
	 * New method that converts cardinal direction into values that one can add to the current position; extremely useful
	 * for sensing in certain directions. If North, it returns a list of size 2 [0,1], where each is added to the x and y 
	 * coordinate respectively in order to scan in that direction!
	 * @param direction
	 * @return
	 */
	public static int[] directionConverter(CardinalDirection direction) {
		if (direction==CardinalDirection.North) {
				return new int[] {0, 1};
		}
		if (direction==CardinalDirection.South) {
			return new int[] {0, -1};
	}
		if (direction==CardinalDirection.East) {
			return new int[] {1, 0};
	}
		if (direction==CardinalDirection.West) {
			return new int[] {-1, 0};
	}
		else{
				return null;
		}
	}
	/**
	 * Updates the position!!! Very straightforward. I kept having crashes and this seemed to solve the crashing.
	 */
	private void update_position() {
		try {
			position = getCurrentPosition();
		} catch (Exception e) {
		}
	}
	@Override
	public void setSensors(String sensors) {
		String sensors_set = sensors;
		this.setMaze(maze);
    	boolean[] sensor_array = new boolean[4];
    	char[] converted = sensors_set.toCharArray();
    	for (int i=0; i<converted.length; i++) {
    		if (converted[i] == '1') {
    			sensor_array[i] = true;
    		}
    		else if (converted[i] == '0') {
    			sensor_array[i] = false;
    		}
    	}
    	if (sensor_array.length != 4) {
    		boolean[] default_array = {true, true, true, true};
    		this.sensors = default_array;
    	}
    	else {
    	this.sensors = sensor_array;
    	}
    	//F L R B
    	
    	//F
    	if (this.sensors[0] == true) {
    		sensor_front = new ReliableSensor();
    	}
    	else {
    		sensor_front = new UnReliableSensor(Direction.FORWARD, control, this);
    	}
    	//L
    	if (this.sensors[1] == true) {
    		sensor_left = new ReliableSensor();
    	}
    	else {
    		sensor_left = new UnReliableSensor(Direction.LEFT, control, this);
    	}
    	//R
    	if (this.sensors[2] == true) {
    		sensor_right = new ReliableSensor();
    	}
    	else {
    		sensor_right = new UnReliableSensor(Direction.RIGHT, control, this);
    	}
    	//B
    	if (this.sensors[3] == true) {
    		sensor_back = new ReliableSensor();
    	}
    	else {
    		sensor_back = new UnReliableSensor(Direction.BACKWARD, control, this);
    	}
		sensor_left.setMaze(maze);
		sensor_right.setMaze(maze);
		sensor_back.setMaze(maze);
		sensor_front.setMaze(maze);
    	
	}



	
	

}
