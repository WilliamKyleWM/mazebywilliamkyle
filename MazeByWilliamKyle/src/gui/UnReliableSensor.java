/**
 * CRC
 * Class UnReliableSensor
 * Extends ReliableSensor and Implements Runnable
 * Responsibilities:
 * 				moving, rotating, and jumping the robot around the maze.
 * Collaborates: 
 * 				with ReliableRobot to sense the distance from obstacles
 *
 */

package gui;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import generation.CardinalDirection;
import generation.Maze;
import gui.Robot.Direction;

/**
 * @Author: WilliamKyle
 */
public class UnReliableSensor extends ReliableSensor implements Runnable {
	Maze maze;	
	public boolean sensor = false;
	private Robot robot;
	private Direction direction;
	Controller control;
	private AtomicBoolean run = new AtomicBoolean(false);

	public UnReliableSensor(Direction direction, Controller control, Robot robot) {
		this.direction = direction;
		this.control = control;
		this.robot = robot;
	}
	
	@Override
	public int distanceToObstacle(int[] currentPosition, CardinalDirection currentDirection, float powersupply)
			throws Exception {
		//run();
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		if (powersupply >= EnergyCost.dist_sense) {
			int[] start_position = currentPosition;
						//Finding absolute direction
			CardinalDirection[] hold = {CardinalDirection.North, CardinalDirection.East, CardinalDirection.South, CardinalDirection.West};
			
			int count = 0;
			if (currentDirection == CardinalDirection.North) {
				currentDirection = CardinalDirection.South;
			}
			else if (currentDirection == CardinalDirection.South) {
				currentDirection = CardinalDirection.North;
			}
			int[] approach = directionConverter(currentDirection);
			if (currentDirection == CardinalDirection.North) {
				currentDirection = CardinalDirection.South;
			}
			else if (currentDirection == CardinalDirection.South) {
				currentDirection = CardinalDirection.North;
			}
			while (!maze.hasWall(start_position[0], start_position[1], currentDirection)) {
				currentPosition[0] += approach[0];
				currentPosition[1] += approach[1];
				
				if (!maze.isValidPosition(currentPosition[0], currentPosition[1])) {
					return Integer.MAX_VALUE;
				}
				count +=1;
							}
			return count;
		}
			else {
				control.robot.setStopped(true);
				return -1;
			}
	}

	@Override
	public void setMaze(Maze maze) {
		// TODO Auto-generated method stub
		this.maze = maze;
	}

	@Override
	public void setSensorDirection(Direction mountedDirection) {
		if (mountedDirection == null) {
			throw new IllegalArgumentException();
		}
		if (mountedDirection == Direction.BACKWARD) {
			this.direction = Direction.BACKWARD;
		}
		else if (mountedDirection == Direction.FORWARD) {
			this.direction = Direction.FORWARD;
		}
		else if (mountedDirection == Direction.LEFT) {
			this.direction = Direction.LEFT;
		}
		else if (mountedDirection == Direction.RIGHT) {
			this.direction = Direction.RIGHT;
		}
	}

	@Override
	public float getEnergyConsumptionForSensing() {
		// TODO Auto-generated method stub
		return EnergyCost.dist_sense;
	}

	@Override
	public void startFailureAndRepairProcess(int meanTimeBetweenFailures, int meanTimeToRepair)
			throws UnsupportedOperationException {
		Thread holder = new Thread(this);
		// TODO Auto-generated method stub
		if (!robot.sensorWorks(direction)) {
			try {
				holder.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sensor = true;
			robot.sensorFix(direction);
			System.out.println("Sensor fixed");
			}
			
			else if (robot.sensorWorks(direction)) {
				try {
					holder.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				sensor = false;
				robot.sensorFail(direction);
				System.out.println("Sensor failed");
			}
	}

	@Override
	public void stopFailureAndRepairProcess() throws UnsupportedOperationException {
		
	}

	@Override
	public void run() {
		while (run.get()) {
			try{
				Thread.sleep(4000);
				robot.sensorFail(direction);
				Thread.sleep(2000);
				robot.sensorFix(direction);
				
				
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				run.set(false);
			}
		}
		}
	

/**
 * Copied from ReliableRobot over.
 * @param direction
 * @return
 */
public static int[] directionConverter(CardinalDirection direction) {
	if (direction==CardinalDirection.North) {
			return new int[] {0, 1};
	}
	else if (direction==CardinalDirection.South) {
		return new int[] {0, -1};
}
	else if (direction==CardinalDirection.East) {
		return new int[] {1, 0};
}
	else if (direction==CardinalDirection.West) {
		return new int[] {-1, 0};
}
	else{
			return null;
	}
}

}
