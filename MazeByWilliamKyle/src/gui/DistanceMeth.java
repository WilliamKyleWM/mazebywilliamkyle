package gui;

import java.util.Arrays;
import java.util.List;

import generation.CardinalDirection;
import gui.Robot.Direction;
import gui.Robot.Turn;

public class DistanceMeth {
	/**
	 * Converts direction to turn values; ie; Backward to around.
	 * Very basic private method that I needed for one specific case in drive2exit and drive1step2exit.
	 * @param direction
	 * @return
	 */
	public Turn directionToTurn(Direction direction) {
		if (direction == Direction.LEFT) {
			return Turn.LEFT;
		}
		else if (direction == Direction.RIGHT) {
			return Turn.RIGHT;
		}
		else {
			return Turn.AROUND;
		}
		
	}
	
	/**
	 * New method to return opposite north and south
	 * @param direction
	 * @return
	 */
	public CardinalDirection directionSwitcher(CardinalDirection direction) {

		if (direction == CardinalDirection.North) {
			return CardinalDirection.South;
		}
		else if (direction == CardinalDirection.South) {
			return CardinalDirection.North;
		}
		else if (direction == CardinalDirection.West) {
			return CardinalDirection.West;
		}
		else if (direction == CardinalDirection.East){
			return CardinalDirection.East;
		}
		else {
			return null;
		}
	}
	
	
	/**
	 * This converts two cardinal directions, the current and the one specified direction, and provides the direction in
	 * 'direction' form (left, right, etc.). Very useful conversion.
	 * @param tell
	 * @param spec_direc
	 * @return
	 */
	public Turn directionSelectorCardinal(CardinalDirection start_direct, CardinalDirection direction) {
		if (((start_direct == CardinalDirection.East)&&(direction == CardinalDirection.East))||((start_direct == CardinalDirection.West)&&(direction==CardinalDirection.West))) {
			System.out.println("Forward");
		}
		else if (((start_direct == CardinalDirection.North) && (direction == CardinalDirection.North)) || ((start_direct == CardinalDirection.South) &&(direction == CardinalDirection.South))){
			System.out.println("Forward");
			
		}
		else if (((start_direct == CardinalDirection.North)&&(direction == CardinalDirection.South))||((start_direct == CardinalDirection.South)&&(direction==CardinalDirection.North))) {
			System.out.println("Around");
			return (Turn.AROUND);
		}
		//Backwards
		else if (((start_direct == CardinalDirection.West) && (direction == CardinalDirection.East)) || ((start_direct == CardinalDirection.East) &&(direction == CardinalDirection.West))){
			System.out.println("Around");
			return (Turn.AROUND);
			
		}
		//Right
		else if (((start_direct == CardinalDirection.South) && (direction == CardinalDirection.West)) || ((start_direct == CardinalDirection.West) &&(direction == CardinalDirection.North))){
			System.out.println("LEFT");
			return (Turn.LEFT);
		}
		else if (((start_direct == CardinalDirection.North) && (direction == CardinalDirection.East)) || ((start_direct == CardinalDirection.East) &&(direction == CardinalDirection.South))){
			System.out.println("LEFT");
			return (Turn.LEFT);
		}
		//Left
		else if (((start_direct == CardinalDirection.East) && (direction == CardinalDirection.North)) || ((start_direct == CardinalDirection.North) &&(direction == CardinalDirection.West))){
			System.out.println("RIGHT");
			return (Turn.RIGHT);
		}
		else if (((start_direct == CardinalDirection.West) && (direction == CardinalDirection.South)) || ((start_direct == CardinalDirection.South) &&(direction == CardinalDirection.East))){
			System.out.println("RIGHT");
			return (Turn.RIGHT);
		}
		else {
			System.out.println("directionSelector failed!");
			
		}
		return null;
	}
	
	/**
	 * New method that converts cardinal direction into values that one can add to the current position; extremely useful
	 * for sensing in certain directions. If North, it returns a list of size 2 [0,1], where each is added to the x and y 
	 * coordinate respectively in order to scan in that direction!
	 * @param direction
	 * @return
	 */
	static int[] directionConverter(CardinalDirection direction) {
		if (direction==CardinalDirection.North) {
				return new int[] {0, 1};
		}
		if (direction==CardinalDirection.South) {
			return new int[] {0, -1};
	}
		if (direction==CardinalDirection.East) {
			return new int[] {1, 0};
	}
		if (direction==CardinalDirection.West) {
			return new int[] {-1, 0};
	}
		else{
				return null;
		}
	}
	
	/**
	 * This method takes the current direction and converts it into a cardinal direction! Very, very handy.
	 * @param direction
	 * @return
	 */
	public CardinalDirection absoluteDirection(Robot robot, Direction direction) {
		CardinalDirection[] dir = {CardinalDirection.North, CardinalDirection.East, CardinalDirection.South, CardinalDirection.West};
		List<CardinalDirection> directions = Arrays.asList(dir);
		
		int cur = directions.indexOf(robot.getCurrentDirection());
		
		if (direction == Direction.FORWARD) {
				return directions.get(cur);
		}
		else if (direction == Direction.LEFT) {
				return directions.get((cur + 1) % 4);
		}
		else if (direction == Direction.BACKWARD) {
				return directions.get((cur + 2) % 4);
		}
			else if (direction == Direction.RIGHT) {
				return directions.get((cur + 3) % 4);
			}
			else{
				return null;
			}
		}
	

}
