package generation;

import generation.Wallboard;
import generation.CardinalDirection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class MazeBuilderEller extends MazeBuilder implements Runnable {

    public cell_listings cell_list;
	public int[][] list_placement;

	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	
	
	//I have my methods documented in the pseudocode version of MazeBuilderEller.

	@Override
	protected void generatePathways() {		
		list_placement = new int[width][height];
		cell_list = new cell_listings();

		for (int y = 0; y < height-1; y++) {
			for (int x = 0; x <width; x++) {
			if (list_placement[x][y] < 0) {
				cell_list.append_list(new int[] {x, y});
			}
		}
			for (int x = 0; x < width - 1; x++) {
			if (list_placement[x][y] != list_placement[x+1][y] && !floorplan.hasWall(x,  y,  CardinalDirection.East)) {
				cell_list.list_combine(list_placement[x][y], list_placement[x+1][y]);
			}
			//random join is decided by random integer between 0 and 10 and if it's less than 4 it joins.
			else if ((list_placement[x][y] != list_placement[x+1][y] && (5 < random.nextIntWithinInterval(0, 10)))) {
				Wallboard wall_board = new Wallboard(x, y, CardinalDirection.East);
				if (floorplan.hasWall(x, y, CardinalDirection.East)) {
					floorplan.deleteWallboard(wall_board);
				}
				cell_list.list_combine(list_placement[x][y], list_placement[x+1][y]);
			}
		}
			for (int x = 0; x < width; x++) {
			if ((list_placement[x][y] != list_placement[x][y+1]) && !(floorplan.hasWall(x, y, CardinalDirection.South))) {
				cell_list.list_combine(list_placement[x][y], list_placement[x][y+1]);
			}
		}
			
			Set<Integer> brand = new HashSet<Integer>();
			for (int x = 0; x < width; x++) {
				brand.add(list_placement[x][y]);
			}
			
			
		for (int i : brand) {
			int[] item = null;
				
			List<int[]> selected_cells= new ArrayList<int[]>(width);
			for (int x = 0; x < width; x++) {
				if (list_placement[x][y] == i) {
					selected_cells.add(new int[] {x, y});
				}
			}
			if (selected_cells.size() > 0) {
				item = selected_cells.get(random.nextIntWithinInterval(0, selected_cells.size() - 1));
			}
			else {item = null;
			}
			
			if (null != item) {
				int X = item[0]; int Y = item[1];
				Wallboard wall_board = new Wallboard(X, Y, CardinalDirection.South);
				if (floorplan.hasWall(X, Y, CardinalDirection.South)) {
					floorplan.deleteWallboard(wall_board);
				}
				
				if (list_placement[X][Y+1] < 0) { 
					
					cell_list.append_cell(new int[] {X, Y + 1}, list_placement[X][Y]); 
					
					
				}
				
				else if (list_placement[X][Y] != list_placement[X][Y+1]) { 
					cell_list.list_combine(list_placement[X][Y], list_placement[X][Y+1]); 
				}
			}
		}
		}
		for (int x = 0; x < width; x++) {
			if (list_placement[x][height-1] < 0) {
				cell_list.append_list(new int[] {x, height-1});
			}
		}
		for (int x = 0; x < width - 1; x++) {
			if (list_placement[x][height-1] != list_placement[x+1][height-1]) {
				cell_list.list_combine(list_placement[x][height-1], list_placement[x+1][height-1]);
				Wallboard wall_board = new Wallboard(x, height-1, CardinalDirection.East);
				if (floorplan.hasWall(x, height-1, CardinalDirection.East)) {
					floorplan.deleteWallboard(wall_board);
				}
			}
		}
	}
	
	public class cell_listings {
		public LinkedList<Integer> empty_space;
		public ArrayList<HashSet<int[]>> cell_list;
		
		public cell_listings() {
			cell_list = new ArrayList<HashSet<int[]>>(15);
			for (int i = 0; i < 15; i++) cell_list.add(new HashSet<int[]>());
			for (int[] g: list_placement) Arrays.fill(g, -1);
			
			empty_space = new LinkedList<Integer>();
			for (int i = 0; i < 15; i++) {
				empty_space.add(i);
			}
			
				LinkedList<int[][]> room_list = new LinkedList<int[][]>();
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					int[] cell_identity = new int[] {x, y};
					
				boolean cell_found = false;
				
				for (int[][] ecke : room_list) {
				int[] ecke_top_left = ecke[0];
				int[] move_right = ecke[1];
				if (cell_identity[0] >= ecke_top_left[0] && cell_identity[0] <= move_right[0] &&
						cell_identity[1] >= ecke_top_left[1] && cell_identity[1] <= move_right[1]) {
					cell_found = true;
				}
				else{
				cell_found = false;
				}
			}

					if (cell_found) {
						continue;
					}
					else {
						if (floorplan.isInRoom(x, y)) {
							int[] move_right = new int[] {cell_identity[0], cell_identity[1]};
							
							while (move_right[0] < width - 1 && floorplan.isInRoom(move_right[0] + 1, move_right[1])) {
								move_right[0]+=1;
							}
							while (move_right[1] < height - 1 && floorplan.isInRoom(move_right[0], move_right[1] + 1)) {
								move_right[1]+=1;
							}

							room_list.add(new int[][] {cell_identity, move_right});							
							int open = 1;
				    		if (empty_space.size() == 0) {
								cell_list.add(new HashSet<int[]>());
								open = cell_list.size()-1;
							}
							else {open = empty_space.remove();
						}
							for (int w = cell_identity[0]; w <= move_right[0]; w++) {
								for (int z = cell_identity[1]; z <= move_right[1]; z++) {
									list_placement[w][z] = open;
									cell_list.get(list_placement[w][z]).add(new int[] {w, z});
								}
							}
							
						}
					}
				}
			}
		}

		
		public void append_list(int[] cell) {
			int open_cell = 1;
    		if (empty_space.size() == 0) {
				cell_list.add(new HashSet<int[]>());
				open_cell = cell_list.size()-1;
			}
			else {open_cell = empty_space.remove();
		}
			cell_list.set(open_cell, new HashSet<int[]>());
			cell_list.get(open_cell).add(cell);
			list_placement[cell[0]][cell[1]] = open_cell;		
		}
		
		public int[] locate_ecke(int[] ecke_top_left) {
			int[] cell = new int[] {ecke_top_left[0], ecke_top_left[1]};
			while (cell[0] < width - 1 && floorplan.isInRoom(cell[0] + 1, cell[1])) {
				cell[0] += 1;
			}
			while (cell[1] < height - 1 && floorplan.isInRoom(cell[0], cell[1] + 1)) {
				cell[1] += 1;
			}
			return cell;
		}
		
		public  void list_combine(int index1, int index2) {
			if (cell_list.get(index1).size() > cell_list.get(index2).size()) {
				cell_list.get(index1).addAll(cell_list.get(index2));
				for (int[] i : cell_list.get(index2)) {
					list_placement[i[0]][i[1]] = index1;
				}
				cell_list.set(index2, new HashSet<int[]>());
				empty_space.add(index2);
			}
			else {
				cell_list.get(index2).addAll(cell_list.get(index1));
				for (int[] i: cell_list.get(index1)) {
					list_placement[i[0]][i[1]] = index2;
				}
				cell_list.set(index1, new HashSet<int[]>());
				empty_space.add(index1);
			}
		}
		
		public void append_cell(int[] cell, int set) {
			int newSet = list_placement[cell[0]][cell[1]];
			if (newSet < 0) {
				list_placement[cell[0]][cell[1]] = set;
				cell_list.get(set).add(cell);
			}
			else {
				list_combine(set, newSet);
			}
		}

	}

}