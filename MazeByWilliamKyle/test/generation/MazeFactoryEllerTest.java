package generation;
import static org.junit.Assert.*;
import generation.CardinalDirection;
import generation.Floorplan;
import generation.Order;
import generation.Order.Builder;
import generation.StubOrder;

import org.junit.Before;
import org.junit.Test;


public class MazeFactoryEllerTest{
	protected Maze maze;
	protected Distance dists;
	protected Builder builder = Builder.Eller;
	protected int skill = 0;

	/**
	 * Just to see if null or not!
	 * 
	 */
	@Test
	public final void testMazeEller(){
		StubOrder order = new StubOrder(builder, skill);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		assertNotNull(factr);
	}
	
	@Test
	public final void testNoClosedRooms() {
		StubOrder order = new StubOrder(builder, skill);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		for (int i=0; i<maze.getWidth();i++) {
			for (int y = 0; y < maze.getHeight(); y++) {
				assertTrue(dists.getDistanceValue(i, y)!= Distance.INFINITY);
			}
		}
	}
	
    @Test
    public final void testEverywhereExit() {
    	StubOrder order = new StubOrder(builder, skill);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		System.out.println(dists.getExitPosition());
		for (int i = 0; i< maze.getWidth(); i++) {
			for (int y = 0; y<maze.getHeight(); y++) {
				assertTrue(dists.getDistanceValue(i, y) != Distance.INFINITY);
			}
		}
    }
	
	@Test
	public final void testSame() {
		StubOrder order = new StubOrder(builder, skill);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		Maze maze2;
		maze2 = order.getMaze();
		assertTrue(maze.getFloorplan()==maze2.getFloorplan());
		
	}
	
	@Test
	public final void testSameCellsArbitaryValues() {
		StubOrder order = new StubOrder(builder, skill);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		Maze maze2;
		maze2 = order.getMaze();
		for (int x = 0; x<maze.getWidth();x++) {
			for (int y = 0; y<maze.getHeight();y++) {
		assertTrue(maze.getMazedists().getDistanceValue(x, y)==maze2.getMazedists().getDistanceValue(x, y));
			}
		}
	}
}