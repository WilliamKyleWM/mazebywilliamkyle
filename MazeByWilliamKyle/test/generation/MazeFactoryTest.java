
package generation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import generation.Order.Builder;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;
import generation.MazeFactory;
import gui.Constants;
import gui.Controller;
import gui.MazePanel;
import gui.SimpleScreens;
import gui.StateGenerating;
import junit.framework.Assert;
import generation.MazeBuilder;

public class MazeFactoryTest{
	protected Maze maze;
	protected Distance dists;
	protected Builder builder = Builder.DFS;
	protected int skillLevel = 0;



	/**
	 * Just to see if null or not!
	 * 
	 */
	@Test
	public final void testMaze(){
		StubOrder order = new StubOrder(builder, skillLevel);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		System.out.print(factr);
		assertNotNull(factr);
	}
	
	/**
	 * Test the cancel method in MazeFactory
	 * 
	 */
	@Test
	public final void testCancelOrder(){
		StubOrder order = new StubOrder(builder, skillLevel);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.cancel();
		assertNotNull(factr);
		//hopefully doesn't return error
	}
	
	@Test
	public final void testgetSeed(){
		Factory factr = new MazeFactory();
		Order order = new StubOrder();
		factr.order(order);
		factr.waitTillDelivered();
		assertTrue(order.getSeed()==13);
		//hopefully doesn't return error
	}
	


    //ONLY WORKS WITH 0 skill level because skill influences height :)
    @Test
    public final void testHeight() {
    	StubOrder order = new StubOrder(builder, skillLevel);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		assertTrue(maze.getHeight() == 4);
    }
    @Test
    public final void testWidth() {
    	StubOrder order = new StubOrder(builder, skillLevel);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		assertTrue(maze.getWidth() == 4);
    }
   

    /*This test is here to test that the the minDistance works correctly. This is
     * vital for finding the exit!.*/
    @Test
    public final void testMinDistance() {
    	Order order = new StubOrder();
		MazeBuilder build = new MazeBuilder();
		build.buildOrder(order);
		int[][] distances = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		build.dists.setAllDistanceValues(distances);
		assertTrue(build.dists.getMinDistance()==1);
    }

    
	 /**
    This test checks that you can use the set, get distance method in mazebuilder!
    */
   @Test
   public final void testSetGetDistances() {
   		Order order = new StubOrder();
		MazeBuilder build = new MazeBuilder();
		build.buildOrder(order);
		int[][] distances = {{0,1,2,3},{4,5,6,7},{8,9,10,11},{12,13,14,15}};
		build.dists.setAllDistanceValues(distances);
		for (int x=0; x<4;x=x+1) {
			for (int y=0; y<4;y++) {
		assertTrue(build.dists.getDistanceValue(x, y)==((y)+(x*4)));
                }
		}
		}

    /**
     Using mazedists once again.
     For i in width for y in height:
     	if cells(x,y) all have null mazedist values
     		Exit does not exist!
     */
    @Test
    public final void testExitExists() {
    	/* using dists, look for*/
    }
    
    @Test
    public final void testMaxDistance() {
   		Order order = new StubOrder();
		MazeBuilder build = new MazeBuilder();
		build.buildOrder(order);
		int[][] distances = {{0,1,2,3},{4,5,6,7},{8,9,10,11},{12,13,14,15}};
		build.dists.setAllDistanceValues(distances);
		assertTrue(build.dists.getMaxDistance()==15);
		int[][] newdistances = {{1000,1,2,3},{4,99,6,7},{8,9,293,11},{12,442,14,15}};
		build.dists.setAllDistanceValues(newdistances);
		assertTrue(build.dists.getMaxDistance()==1000);
		}
	
    @Test
    public final void testMinDistanceInitialized() {
    	StubOrder order = new StubOrder(builder, skillLevel);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		System.out.println(dists.getExitPosition());
		assertTrue(dists.getMinDistance() == 1);
		}
    
    @Test
    public final void testEverywhereExit() {
    	StubOrder order = new StubOrder(builder, skillLevel);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		System.out.println(dists.getExitPosition());
		for (int i = 0; i< maze.getWidth(); i++) {
			for (int y = 0; y<maze.getHeight(); y++) {
				assertTrue(dists.getDistanceValue(i, y) != Distance.INFINITY);
			}
		}
		}
    
    @Test
    public final void testExitSingle() {
    	StubOrder order = new StubOrder(builder, skillLevel);
		Factory factr = new MazeFactory();
		factr.order(order);
		factr.waitTillDelivered();
		maze = order.getMaze();
		dists = maze.getMazedists();
		int checker = 1;
		for (int i = 0; i< maze.getWidth(); i++) {
			for (int y = 0; y<maze.getHeight(); y++) {
				if (dists.getDistanceValue(i, y) == 1) {
					checker +=1;
				}
			}
		}
		assertTrue(checker ==2);
		}
		
}