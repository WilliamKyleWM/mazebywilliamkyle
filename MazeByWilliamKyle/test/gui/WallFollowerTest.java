package gui;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.Maze;
import generation.Order;


/**
 * @Author: WilliamKyle
 */
class WallFollowerTest {
	RobotDriver wizard;
	Controller control;
	Robot robot;
	Maze maze;
	RobotDriver driver;
	
	@Before
	public void setUp() {
		control = new Controller();
		robot = new UnReliableRobot();
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,null);
		control.setBuilder(Order.Builder.DFS);
	}

	/**
	 * The robot succeeds in using the wallfollower algorithm! 4 times seems 
	 * like a good amount to test that it works!
	 */
	@Test
	public void testAvoidsWallboardsandExits() {
		for (int i = 0; i<4; i++) {
		control = new Controller();
		robot = new UnReliableRobot();
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"1111");
		control.setBuilder(Order.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {}
			}
			assertTrue(control.currentState instanceof StateWinning);
		}
	}	
		/**
		 * Tests that the odometer works correctly! Starting at 0.
		 */
		@Test
		public void testOdometer() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,null);
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			robot.getOdometerReading();
			assertTrue(robot.getOdometerReading()==0);
		}
		/**
		 * Tests that the jump method works appropriately!
		 */
		@Test
		public void testJump() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,null);
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);

			try {
				robot.jump();
				int[] pos = robot.getCurrentPosition();
				assertTrue(maze.isValidPosition(pos[0], pos[1]));
				assertTrue(robot.getBatteryLevel()==(3500-EnergyCost.jump));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		/**
		 * Tests that not only does the robot make it to the exit, but 
		 * it doesn't crash along the way!
		 */
		@Test
		public void testWallFollowerDoesNotCrash() {
			for (int i = 0; i<4; i++) {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"1111");
			control.setBuilder(StubOrder.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
					} catch (InterruptedException e) {}
				}
			assertFalse(robot.hasStopped());
			assertTrue(control.currentState instanceof StateWinning);
			}
}
		/**
		 * Tests that the WallFollower new drive2exitefficient method is more efficient!
		 */
		@Test
		public void testWallFollowerEfficient() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"1111");
			control.setBuilder(StubOrder.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			float bat_lev_wiz = 0;
				while (!(control.currentState instanceof StateWinning)) {
					try {
						Thread.sleep(20);
						bat_lev_wiz = robot.getBatteryLevel();
					} catch (InterruptedException e) {}
				}
			control = new Controller();
			Robot robot2 = new UnReliableRobot();
			RobotDriver driver2 = new WallFollower();
			control.setEfficient(true);
			driver2.setRobot(robot2);
			control.setRobotAndDriverAndSensors(robot2, driver2,"1111");
			control.setBuilder(StubOrder.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			float bat_lev_wizjump = 0;
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
					bat_lev_wizjump = robot2.getBatteryLevel();
					
				} catch (InterruptedException e) {}
			}
			assertTrue(bat_lev_wizjump>bat_lev_wiz);
				
			}
}
