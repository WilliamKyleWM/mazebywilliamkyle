package gui;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.Order;

/**
 * 
 * @author WilliamKyle
 *
 */
class ReliableRobotTest {
	Controller control;
	Robot robot;
	
	@Before
	public void setUp() {
		control = new Controller();
		control.setFileName("test\\data\\input.xml");
		control.start();
		
		robot = new ReliableRobot();
		robot.setController(control);
	}

	@Test
	public final void testOdometer() {
		robot = new ReliableRobot();
		robot.setController(control);
		System.out.println(robot.getOdometerReading());
		assertEquals(0,robot.getOdometerReading());
	}
	


	
}
