package gui;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.Maze;
import generation.Order;


/**
 * @Author: WilliamKyle
 */
class ReliableSensorTest {
	Controller control;
	Robot robot;
	Maze maze;
	RobotDriver driver;
	
	@Before
	public void setUp() {
		control = new Controller();
		robot = new UnReliableRobot();
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"0000");
		control.setBuilder(Order.Builder.DFS);
	}

	
	@Test
	public void testAvoidsWallboardsandExits() {
		control = new Controller();
		robot = new UnReliableRobot();
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"0000");
		control.setBuilder(Order.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {}
			}
			assertTrue(control.currentState instanceof StateWinning);
		}

		@Test
		public void testOdometer() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"0000");
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			robot.getOdometerReading();
			assertTrue(robot.getOdometerReading()==0);
		}
	
		@Test
		public void testJump() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"0000");
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);

			try {
				robot.jump();
				int[] pos = robot.getCurrentPosition();
				assertTrue(maze.isValidPosition(pos[0], pos[1]));
				assertTrue(robot.getBatteryLevel()==(3500-EnergyCost.jump));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		@Test
		public void testWallFollowerDoesNotCrash() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"0000");
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
					} catch (InterruptedException e) {}
				}
			assertFalse(robot.hasStopped());
			assertTrue(control.currentState instanceof StateWinning);

}
}
