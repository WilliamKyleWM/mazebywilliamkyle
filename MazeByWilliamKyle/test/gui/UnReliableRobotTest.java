package gui;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.Maze;
import generation.Order;
import gui.Robot.Direction;


/**
 * @Author: WilliamKyle
 */
class UnReliableRobotTest {
	Controller control;
	Robot robot;
	Maze maze;
	RobotDriver driver;
	
	@Before
	public void setUp() {
		control = new Controller();
		robot = new UnReliableRobot();
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"1111");
		control.setBuilder(Order.Builder.DFS);
	}

	
	@Test
	public void testAvoidsWallboardsandExits() {
		control = new Controller();
		robot = new UnReliableRobot();
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"1111");
		control.setBuilder(Order.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {}
			}
			assertTrue(control.currentState instanceof StateWinning);
		}

		@Test
		public void testOdometer() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"1111");
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			robot.getOdometerReading();
			assertTrue(robot.getOdometerReading()==0);
		}
	
		
		@Test
		public void testSensorSetWorks() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"1111");
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			assertTrue(robot.sensorWorks(Direction.BACKWARD));
			assertTrue(robot.sensorWorks(Direction.FORWARD));
			assertTrue(robot.sensorWorks(Direction.LEFT));
			assertTrue(robot.sensorWorks(Direction.RIGHT));
}

		@Test
		public void testSensorFail() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"1111");
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			robot.sensorFail(Direction.LEFT);
			assertFalse(robot.sensorWorks(Direction.LEFT));

		}
		
		@Test
		public void testSensorFailAll() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new WallFollower();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"1111");
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			robot.sensorFail(Direction.LEFT);
			robot.sensorFail(Direction.RIGHT);
			robot.sensorFail(Direction.FORWARD);
			robot.sensorFail(Direction.BACKWARD);
			assertFalse(robot.sensorWorks(Direction.BACKWARD));
			assertFalse(robot.sensorWorks(Direction.FORWARD));
			assertFalse(robot.sensorWorks(Direction.LEFT));
			assertFalse(robot.sensorWorks(Direction.RIGHT));

}
}
