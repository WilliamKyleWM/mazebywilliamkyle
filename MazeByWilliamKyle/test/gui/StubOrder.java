package gui;

import generation.Order.Builder;
import gui.Controller;
import gui.DefaultState;
import gui.MazeFileReader;
import gui.MazePanel;
import gui.SimpleScreens;
import gui.StatePlaying;
import gui.Constants.UserInput;
import generation.Factory;
import generation.Floorplan;
import generation.Maze;
import generation.MazeFactory;
import generation.Order;

public class StubOrder extends DefaultState implements Order {
	
	public StubOrder(Builder builder, int skill) {
		this.builder = builder;
		this.skillLevel = skill;
	}
	
	SimpleScreens view;
    MazePanel panel;
    Controller control;
    // Filename if maze is loaded from file, can be null
    private String filename;
    
    // about the maze and its generation
    private int seed; // the seed value used for the random number generator
    private int skillLevel; // user selected skill level, i.e. size of maze
    private Builder builder; // selected maze generation algorithm
    private boolean perfect; // selected type of maze, i.e. 
    // perfect == true: no loops, i.e. no rooms
    // perfect == false: maze can support rooms
   
    // The factory is used to calculate a new maze configuration
    // The maze is computed in a separate thread which makes 
    // communication with the factory slightly more complicated.
    // Check the factory interface for details.
    protected Factory factory;
    // The maze configuration produced by the factory
    public Maze mazeConfig; 
    public Maze mazeCon;

    private int percentdone;        // describes progress during generation phase

    boolean started;
    
	 public StubOrder() {
	        filename = null;
	        factory = new MazeFactory() ;
	        skillLevel = 2; // default size for maze
	        builder = Order.Builder.DFS; // default algorithm
	        perfect = true; // default: maze can have rooms
	        percentdone = 0;
	        started = false;
	        seed = 13; // default: an arbitrary fixed value
	    }
	
	 
	 
	
	
	 @Override
	    public void setFileName(String filename) {
	        this.filename = filename;  
	    }
	    @Override
	    public void setSkillLevel(int skillLevel) {
	        this.skillLevel = skillLevel;
	    }
	    @Override
	    public void setBuilder(Builder builder) {
	        this.builder = builder;
	    }
	    @Override
	    public void setPerfect(boolean isPerfect) {
	        perfect = isPerfect;
	    }
	    @Override
		public void setSeed(int seed) {
	        this.seed = seed;  
	    }
	    @Override
	    public int getSkillLevel() {
	        return skillLevel;
	    }
	    @Override
	    public Builder getBuilder() {
	        return builder;
	    }
	    @Override
	    public boolean isPerfect() {
	        return true;
	    }
	    @Override
	    public int getSeed() {
	    	return seed;
	    }
	    public int getPercentDone() {
	        return percentdone;
	    }

		@Override
		public void deliver(Maze mazeConfig)  { //
        this.mazeConfig = mazeConfig;
    }
		
		public Maze getMaze() {
			return mazeConfig;
			
		}

		@Override
		public void updateProgress(int percentage) {
			if (this.percentdone < percentage && percentage <= 100) {
	            this.percentdone = percentage;
	        }
			
		}
		
	    

}