package gui;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.Maze;
import generation.Order;


/**
 * @Author: WilliamKyle
 */
class WizardTest {
	RobotDriver wizard;
	Controller control;
	Robot robot;
	Maze maze;
	RobotDriver driver;
	
	@Before
	public void setUp() {
		control = new Controller();
		robot = new ReliableRobot();
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,null);
		control.setBuilder(Order.Builder.DFS);
		control.turnOffGraphics();
	}
	
	@Test
	public void testAvoidsWallboardsandExits() {
		control = new Controller();
		robot = new ReliableRobot();
		robot.setController(control);
		driver = new Wizard();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,null);
		control.setBuilder(Order.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
			
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {}
			}
			assertTrue(control.currentState instanceof StateWinning);
		}
	
		@Test
		public void testOdometer() {
			control = new Controller();
			robot = new ReliableRobot();
			robot.setController(control);
			driver = new Wizard();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,null);
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			robot.getOdometerReading();
			assertTrue(robot.getOdometerReading()==0);
		}
		
		@Test
		public void testNextClosestNeighbor() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new Wizard();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"1111");
			control.switchFromTitleToGenerating(0);
			maze = control.getMazeConfiguration();
			try {
				int[] pos = robot.getCurrentPosition();
				driver.setMaze(maze);
				driver.drive1Step2Exit();
				int[] pos2 = robot.getCurrentPosition();
				assertTrue(maze.getDistanceToExit(pos2[0], pos2[1])==maze.getDistanceToExit(pos[0], pos[1]-1));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	

}
