package gui;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.CardinalDirection;
import generation.Maze;
import generation.Order;
import generation.Order.Builder;
import gui.Controller;
import gui.DistanceSensor;
import gui.Robot;
import gui.RobotDriver;
import gui.UnReliableRobot;
import gui.UnReliableSensor;
import gui.WallFollower;
import gui.Robot.Direction;

/**
 * @Author: WilliamKyle
 */
class UnReliableSensorTest {
	Controller control;
	Robot robot;
	Maze maze;
	RobotDriver driver;
	DistanceSensor sensor;
	
	@Before
	public void setUp() {
		control = new Controller();
		robot = new UnReliableRobot();
		sensor = new UnReliableSensor(Direction.LEFT,control,robot);
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"0000");
		control.setBuilder(StubOrder.Builder.DFS);
	}
	/**
	 * Tests that the sensor works when set to the left
	 */
	@Test
	public void testSensorLeftWorks() {
		control = new Controller();
		robot = new UnReliableRobot();
		sensor = new UnReliableSensor(Direction.LEFT,control,robot);
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"0000");
		control.setBuilder(StubOrder.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
		int[] position = {1,1};
		try {		
			maze = control.getMazeConfiguration();
			sensor.setMaze(maze);
			//I know there is a wall a cell away, so it has to be 1
			assertTrue(sensor.distanceToObstacle(position, CardinalDirection.North, 100)==1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Tests that the sensor operates normally when set to the right
	 */
	@Test
	public void testSensorRightWorks() {
		control = new Controller();
		robot = new UnReliableRobot();
		sensor = new UnReliableSensor(Direction.RIGHT,control,robot);
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"0000");
		control.setBuilder(StubOrder.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
		int[] position = {1,1};
		try {		
			maze = control.getMazeConfiguration();
			sensor.setMaze(maze);
			//I know there is a wall a cell away, so it has to be 1
			assertTrue(sensor.distanceToObstacle(position, CardinalDirection.North, 100)==1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Tests that the sensor operates normally when set to the back
	 */
	@Test
	public void testSensorBackWorks() {
		control = new Controller();
		robot = new UnReliableRobot();
		sensor = new UnReliableSensor(Direction.BACKWARD,control,robot);
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"0000");
		control.setBuilder(StubOrder.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
		int[] position = {1,1};
		try {		
			maze = control.getMazeConfiguration();
			sensor.setMaze(maze);
			//I know there is a wall a cell away, so it has to be 1
			assertTrue(sensor.distanceToObstacle(position, CardinalDirection.North, 100)==1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Tests that the sensor operates normally when set to the front
	 */
	@Test
	public void testSensorFrontWorks() {
		control = new Controller();
		robot = new UnReliableRobot();
		sensor = new UnReliableSensor(Direction.FORWARD,control,robot);
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,"0000");
		control.setBuilder(StubOrder.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
		int[] position = {1,1};
		try {		
			maze = control.getMazeConfiguration();
			sensor.setMaze(maze);
			//I know there is a wall a cell away, so it has to be 1
			assertTrue(sensor.distanceToObstacle(position, CardinalDirection.North, 100)==1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}