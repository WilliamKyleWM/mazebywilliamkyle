package gui;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import generation.Maze;
import generation.Order;


/**
 * @Author: WilliamKyle
 */
class WizardJumpTest {
	RobotDriver wizard;
	Controller control;
	Robot robot;
	Maze maze;
	RobotDriver driver;
	
	@Before
	public void setUp() {
		control = new Controller();
		robot = new ReliableRobot();
		robot.setController(control);
		driver = new WallFollower();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,null);
		control.setBuilder(Order.Builder.DFS);
	}

	
	@Test
	public void testAvoidsWallboardsandExits() {
		control = new Controller();
		robot = new ReliableRobot();
		robot.setController(control);
		driver = new Wizard();
		driver.setRobot(robot);
		control.setRobotAndDriverAndSensors(robot, driver,null);
		control.setBuilder(Order.Builder.DFS);
		control.start();
		control.switchFromTitleToGenerating(0);
			
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {}
			}
			assertTrue(control.currentState instanceof StateWinning);
		}

	
	
		@Test
		public void testOdometer() {
			control = new Controller();
			robot = new ReliableRobot();
			robot.setController(control);
			driver = new Wizard();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,null);
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			robot.getOdometerReading();
			assertTrue(robot.getOdometerReading()==0);
		}
	
		@Test
		public void testJump() {
			control = new Controller();
			robot = new ReliableRobot();
			robot.setController(control);
			driver = new Wizard();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,null);
			control.setBuilder(Order.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			try {
				robot.jump();
				int[] pos = robot.getCurrentPosition();
				assertTrue(maze.isValidPosition(pos[0], pos[1]));
				assertTrue(robot.getBatteryLevel()==(3500-EnergyCost.jump));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		/**
		 * Tests that the algorithm is more efficient than wizard, by measuring batterylevel
		 * after the traversal.
		 */
		@Test
		public void testMoreEfficientThanWizard() {
			control = new Controller();
			robot = new UnReliableRobot();
			robot.setController(control);
			driver = new Wizard();
			driver.setRobot(robot);
			control.setRobotAndDriverAndSensors(robot, driver,"1111");
			control.setBuilder(StubOrder.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			float bat_lev_wiz = 0;
				while (!(control.currentState instanceof StateWinning)) {
					try {
						Thread.sleep(20);
						bat_lev_wiz = robot.getBatteryLevel();
					} catch (InterruptedException e) {}
				}
			control = new Controller();
			Robot robot2 = new UnReliableRobot();
			RobotDriver driver2 = new WizardJump();
			driver2.setRobot(robot2);
			control.setRobotAndDriverAndSensors(robot2, driver2,"1111");
			control.setBuilder(StubOrder.Builder.DFS);
			control.start();
			control.switchFromTitleToGenerating(0);
			float bat_lev_wizjump = 0;
			while (!(control.currentState instanceof StateWinning)) {
				try {
					Thread.sleep(20);
					bat_lev_wizjump = robot2.getBatteryLevel();
					
				} catch (InterruptedException e) {}
			}
			assertTrue(bat_lev_wizjump>bat_lev_wiz);
				
			}
	

}
